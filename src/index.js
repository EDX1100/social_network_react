import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';
import store from './redux/State';
import './index.css';
import App from './App';
import { BrowserRouter } from 'react-router-dom';

// addMes('HelloWorld')

let rerenderEntireTree = (state) => {
  ReactDOM.render(

      <React.StrictMode>
          <BrowserRouter>
        <App 
          store={store}
          state = {state}
          dispatch= {store.dispatch.bind(store)}
        />
        </BrowserRouter>
      </React.StrictMode>,
      document.getElementById('root' )
    
  );
}

rerenderEntireTree(store.getState());

store.subscribe(rerenderEntireTree);

reportWebVitals();
