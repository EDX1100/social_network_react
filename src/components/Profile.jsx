
const Profile = () => {
    return (
        <div className="profile">
        <img className="profile_photo" src="img/profile/Shape.jpg" alt="photo"/>
        <div className="profile_inner">
            <div className="profile_name_indikator">
                <p className="profile_name">Amilia Luna</p>
                <div className="indikator active"></div>
            </div>
            {/* <!-- /.profile_name_indikator --> */}
            <p className="profile_profession">UI Designer</p>
            <div className="profile_socials">
                <div className="social_item">
                    <img src="img/profile/socials/Combined shape 142.svg" alt="social"/>
                </div>
                <div className="social_item">
                    <img src="img/profile/socials/Combined shape 143.svg" alt="social"/>
                </div>
                <div className="social_item">
                    <img src="img/profile/socials/Icon.svg" alt="social"/>
                </div>
                <div className="social_item">
                    <img src="img/profile/socials/Icon-1.svg" alt="social"/>
                </div>
            </div>
            {/* <!-- /.profile_socials --> */}
            <div className="profile_actions">
                <button className="profile_message">Message</button>
                <button className="profile_more">
                    <img src="img/profile/Rectangle 1.svg" alt="arrow" className="arrow"/>
                </button>
            </div>
            {/* <!-- /.profile_actions --> */}
            <div className="info_username">
                <p className="info_title">Username</p>
                <p className="info_subtitle">@amilia_lu</p>
            </div>
            {/* <!-- /.info_username --> */}
            <div className="info_email">
                <p className="info_title">Email</p>
                <p className="info_subtitle">a-luna@gmail.com</p>
            </div>
            {/* <!-- /.info_email --> */}
            <div className="info_skype">
                <p className="info_title">Skype</p>
                <p className="info_subtitle">amiluna</p>
            </div>
            {/* <!-- /.info_skype --> */}
            <div className="info_timezone">
                <p className="info_title">Timezone</p>
                <p className="info_subtitle">2:21 PM Local time</p>
            </div>
            {/* <!-- /.info_timezone --> */}
        </div>
        {/* <!-- /.profile_inner --> */}
    </div>
    );
}

export default Profile