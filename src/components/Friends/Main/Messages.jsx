import ChannelMessage from "../../Message/Message";

const ChannelMessages = (props) => {
  let messageItems = props.state.messageFriends.map((message) => (
    <ChannelMessage
      userName={message.userName}
      message={message.message}
      message_date={message.message_date}
      message_meridiem={message.message_meridiem}
    />
  ));
  return <div>{messageItems}</div>;
};

export default ChannelMessages;
