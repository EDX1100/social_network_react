import React from "react";
import {
  sendMessageCreator,
  updateNewMessageBodyCreator,
} from "../../../redux/FriendsReducer";

const ChannelMessageInput = (props) => {
  let state = props.store.getState().friendsPage;
  let newMessageBody = state.newMessageBody;
  let onSendMessageClick = () => {
    props.store.dispatch(sendMessageCreator());
  };
  let onNewMessageChange = (event) => {
    let body = event.target.value;
    props.store.dispatch(updateNewMessageBodyCreator(body));
  };

  return (
    <div className="message_input">
      <div className="message_input_inner">
        <button className="attach">
          <img src="img/active_channel/input/Icon-1.svg" alt="img" />
        </button>
        <button className="voice">
          <img
            src="img/active_channel/input/Combined shape 516.svg"
            alt="img"
          />
        </button>
        <textarea
          className="message_input_text"
          placeholder={"Message in" + " " + props.ChannelName}
          value={newMessageBody}
          onChange={onNewMessageChange}
        ></textarea>
        <button className="send_button" onClick={onSendMessageClick}>
          <img src="img/active_channel/input/Icon.svg" alt="img" />
        </button>
      </div>
      {/* <!-- /.message_input_inner --> */}
    </div>
  );
};

export default ChannelMessageInput;
