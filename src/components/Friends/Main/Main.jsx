import ChannelHeader from "../../MaketItems/Header/Header";
import ChannelMessageInput from "./Input";
import ChannelMessages from "./Messages";

const FriendsMain = (props) => {
  return (
    <div>
      <ChannelHeader ChannelName="#private message" />
      <div className="active_channel_scroll">
        <div className="active_channel_main">
          <ChannelMessages state={props.state} />
        </div>
        {/* <!-- /.active_channel_main --> */}
        <ChannelMessageInput
          ChannelName="#private message"
          dispatch={props.dispatch}
          store={props.store}
        />
      </div>
    </div>
  );
};

export default FriendsMain;
