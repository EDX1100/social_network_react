import { NavLink } from "react-router-dom";

// Компонента
const FriendItem = (props) => {
  return (
    <div className="friend_item">
      <div className="indikator active"></div>
      <img
        className="avatar"
        src="img/nomand list/friends/Photo.png"
        alt="avatar"
      />
      <NavLink to="friends/" className="friend_name">
        {props.name}
      </NavLink>
    </div>
  );
};

const Friends = (props) => {
  let FriendItems = props.state.FriendsData.map((friend) => (
    <FriendItem name={friend.name} nameURL={friend.id} />
  ));

  return (
    <div className="friends">
      <div className="friends_header">
        <p className="friends_title">FRIENDS</p>
        <p className="friends_count">6</p>
      </div>
      {/* <!-- /.friends_header --> */}
      {FriendItems}
    </div>
  );
};

export default Friends;
