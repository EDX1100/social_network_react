import active_channel_header from "./Header.module.css";

const ChannelHeader = (props) => {
  return (
    <div className={active_channel_header.header}>
      <div className="active_channel_left">
        <p className="active_channel_name">{props.ChannelName}</p>
        <img
          className="favorite"
          src="img/active_channel/Combined shape 364.svg"
          alt="favorite"
        />
      </div>
      {/* <!-- /.active-channel_left --> */}
      <div className="active_channel_right">
        <img
          src="img/active_channel/Combined shape 363.svg"
          alt="people"
          className="people"
        />
        <p className="active_channel_count_people">1,093</p>
        <div className="search_inner">
          <input
            type="search"
            className="active_channel_search"
            placeholder="Search..."
          />
          <button className="search_button">
            <img src="img/active_channel/Combined shape 352.svg" alt="search" />
          </button>
        </div>
        {/* <!-- /.search_inner --> */}
        <div className="notification">
          <img
            src="img/active_channel/Combined shape 366.svg"
            alt="bell"
            className="bell"
          />
          <div className="red_notification"></div>
        </div>
        {/* <!-- /.notification --> */}
        <button className="active_channel_more">
          <div className="active_channel_more_circle"></div>
          <div className="active_channel_more_circle"></div>
          <div className="active_channel_more_circle"></div>
        </button>
      </div>
      {/* <!-- /.active_channel_right --> */}
    </div>
  );
};

export default ChannelHeader;
