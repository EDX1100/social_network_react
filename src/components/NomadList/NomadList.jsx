import { BrowserRouter, NavLink, Route } from "react-router-dom";
import css from "/Learn/itcam/01/it01/src/App.css";
import nomand_list_header from "./NomandList.module.css";
import Friends from "../Friends/Friends";

const NomandList = (props) => {
  return (
    <div className="nomand_list">
      <div className="nomand_list_container">
        <div className={nomand_list_header.header}>
          <div className="nomand_list_arrow">
            <p className="nomand_title">Nomad List</p>
            <img src="./img/nomand list/Icon.svg" alt="icon" />
          </div>
          {/* <!-- /.nomand_list_arrow --> */}
          <img src="./img/nomand list/shape.svg" alt="shape" />
        </div>
        {/* <!-- /.nomand_list_header --> */}
        <div className="nomand_list_all">
          <img
            src="./img/nomand list/message.svg"
            alt="message"
            className="all"
          />
          <p className="nomand_list_all_name">All treads</p>
        </div>
        {/* <!-- /.nomand_list_all --> */}
        <div className="channels">
          <div className="channels_header">
            <p className="channels_title">CHANNELS</p>
            <p className="channels_count">11</p>
          </div>
          {/* <!-- /.channels_header --> */}
          <div className="channel_items">
            <p className="channel">
              <NavLink to="/general" activeClassName={css.active}>
                # general
              </NavLink>
            </p>
            <p className="channel">
              <NavLink to="/support"># support</NavLink>
            </p>
          </div>
          {/* <!-- /.channel_items --> */}
        </div>
        {/* <!-- /.channels --> */}
        {/* //! Передаём данные */}
        {/* //! Данные пришли из App.js */}
        <Friends state={props.state} />
      </div>
      {/* <!-- /.nomand_list_container --> */}
    </div>
  );
};

export default NomandList;
