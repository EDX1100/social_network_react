const Socials = () => {
    return  (
        <div className="socials">
            <div className="social">
                <img src="./img/socials/1.svg" alt="social" />
            </div>
            {/* <!-- /.social --> */}
            <div className="social">
                <img src="./img/socials/2.svg" alt="social" />
            </div>
            {/* <!-- /.social --> */}
            <div className="social">
                <img src="./img/socials/3.svg" alt="social" />
            </div>
            {/* <!-- /.social --> */}
            <div className="social">
                <img src="./img/socials/4.svg" alt="social" />
            </div>
            {/* <!-- /.social --> */}
            <div className="social">
                <img src="./img/socials/5.svg" alt="social" />
            </div>
            {/* <!-- /.social --> */}
            <div className="social">
                <img src="./img/socials/6.svg" alt="social" />
            </div>
            {/* <!-- /.social --> */}
            <div className="social">
                <img src="./img/socials/7.svg" alt="social" />
            </div>
            {/* <!-- /.social --> */}
            <button className="edit"></button>
        </div>
    );
} 

export default Socials;