import ChannelMessageInput from "./Input";
import ChannelHeader from "../../../MaketItems/Header/Header";
import ChannelMessages from "./Messages";

const SupportChannelMain = (props) => {
  return (
    <div>
      <ChannelHeader ChannelName="#support" />
      <div className="active_channel_scroll">
        <div className="active_channel_main">
          <ChannelMessages state={props.state} />
        </div>
        {/* <!-- /.active_channel_main --> */}
        <ChannelMessageInput
          ChannelName="#support"
          dispatch={props.dispatch}
          newMesText={props.state.newMesText}
        />
      </div>
    </div>
  );
};

export default SupportChannelMain;
