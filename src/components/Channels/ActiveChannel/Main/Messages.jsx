import ChannelMessage from "../../../Message/Message";

const ChannelMessages = (props) => {
  // Здесь были данные, которые вынесены в state

  let messageItems = props.state.messageGeneral.map((message) => (
    <ChannelMessage
      userName={message.userName}
      message={message.message}
      message_date={message.message_date}
      message_meridiem={message.message_meridiem}
    />
  ));

  // Вынесли массив и вызвали его с помощью {messageItems}
  return <div>{messageItems}</div>;
};

export default ChannelMessages;
