import React from "react";
import {
  addMesActionCreator,
  updateNewMesTextActionCreator,
} from "../../../../redux/MessageReducer";

const ChannelMessageInput = (props) => {
  let newMesElement = React.createRef(); //Ссылка на пока не существующий элемент

  let addMes = () => {
    props.dispatch(addMesActionCreator());
  };

  let onMesChange = () => {
    let text = newMesElement.current.value;
    // let action = { type: "UPDATE-NEW-POST-TEXT", newText: text };
    let action = updateNewMesTextActionCreator(text);
    props.dispatch(action); //Изменение state потом отображение данные в поле ввода
  };
  return (
    <div className="message_input">
      <div className="message_input_inner">
        <button className="attach">
          <img src="img/active_channel/input/Icon-1.svg" alt="img" />
        </button>
        <button className="voice">
          <img
            src="img/active_channel/input/Combined shape 516.svg"
            alt="img"
          />
        </button>
        <textarea
          ref={newMesElement} //Указание ссылки на элемент
          className="message_input_text"
          placeholder={"Message in" + " " + props.ChannelName}
          rows="1"
          value={props.newMesText}
          onChange={onMesChange}
        ></textarea>
        <button className="send_button" onClick={addMes}>
          <img src="img/active_channel/input/Icon.svg" alt="img" />
        </button>
      </div>
      {/* <!-- /.message_input_inner --> */}
    </div>
  );
};

export default ChannelMessageInput;
