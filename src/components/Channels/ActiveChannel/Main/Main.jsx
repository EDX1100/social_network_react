import ChannelHeader from "../../../MaketItems/Header/Header";
import ChannelMessageInput from "./Input";
import ChannelMessages from "./Messages";

const ChannelMain = (props) => {
  return (
    <div>
      <ChannelHeader ChannelName="#general" />
      <div className="active_channel_scroll">
        <div className="active_channel_main">
          <ChannelMessages state={props.state} />
        </div>
        {/* <!-- /.active_channel_main --> */}
        <ChannelMessageInput
          ChannelName="#general"
          addMes={props.addMes}
          newMesText={props.state.newMesText}
          updateNewMesText={props.updateNewMesText}
        />
      </div>
    </div>
  );
};

export default ChannelMain;
