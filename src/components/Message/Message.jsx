const ChannelMessage = (props) => {
  let a = `${props.userName}`;

  return (
    <div>
      <div className="user_message">
        <img
          src="img/nomand list/friends/Photo.png"
          alt="avatar"
          className="avatar"
        />
        <div className="user_message_inner">
          <div className="user_message_info">
            <p className="user_name">{props.userName}</p>
            <p className="message_date">{props.message_date}</p>
            <p className="message_meridiem">{props.message_meridiem}</p>
          </div>
          {/* <!-- /.user_message_info --> */}
          <div className="user_message_container">
            <p className="message_text">{props.message}</p>
          </div>
          {/* <!-- /.user_message_container --> */}
        </div>
        {/* <!-- /.user_message_inner --> */}
      </div>
      {/* <!-- /.user_message --> */}
    </div>
  );
};

export default ChannelMessage;
