import './App.css'; //Импорт стилей
import Profile from './components/Profile';
import Socials from './components/Socials';
import NomandList from './components/NomadList/NomadList';
import ChannelMain from './components/Channels/ActiveChannel/Main/Main';
import SupportChannelMain from './components/Channels/SupportChannel/Main/Main';
import { Route } from 'react-router-dom';
import FriendsMain from './components/Friends/Main/Main';

const App = (props) => {
  return (
    <div>
        <section class="window">
            <div class="window_inner">
              <Socials/>
              <NomandList state={props.state.friendsPage}/>
                <div class="active_channel">
                  <Route path="/general" render= {() => <ChannelMain 
                    state={props.state.messagesPage} 
                    dispatch = {props.dispatch} />}/> 
                  <Route path="/support" render ={() => <SupportChannelMain 
                    state={props.state.messagesPage} 
                    dispatch = {props.dispatch}/>}/>
                  <Route path="/friends" render ={() => <FriendsMain 
                    store={props.store}
                    state={props.state.friendsPage} 
                    dispatch = {props.dispatch}/>}/>
                </div>
                {/* <!-- /.active_channel --> */}
              <Profile/>
            </div>
            {/* <!-- /.window_inner --> */}
        </section>
        {/* <!-- /.window --> */}
    </div>
  );
}

export default App;
