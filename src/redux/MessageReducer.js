const ADD_MES = "ADD-MES";
const UPDATE_NEW_MES_TEXT = "UPDATE-NEW-MES-TEXT";

// messagesPage приходит под именем state
const messageReducer = (state, action) => {
  switch (action.type) {
    case ADD_MES:
      let newMes = {
        id: 5,
        userName: "New User",
        message: state.newMesText,
        message_date: "0:00",
        message_meridiem: "PM",
      };
      state.messageSupport.push(newMes);
      state.newMesText = ""; //Очистка поля после добалвения сообщения
      return state;

    case UPDATE_NEW_MES_TEXT:
      state.newMesText = action.newText;
      return state;

    default:
      return state;
  }
};

export const addMesActionCreator = () => ({ type: ADD_MES });
export const updateNewMesTextActionCreator = (text) => ({
  type: UPDATE_NEW_MES_TEXT,
  newText: text,
});

export default messageReducer;
