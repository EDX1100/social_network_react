import friendsReducer from "./FriendsReducer";
import messageReducer from "./MessageReducer";

let store = {
  // Анонимный метод
  _state: {
    messagesPage: {
      messageSupport: [
        {
          id: 1,
          userName: "Jeshua Stout",
          message: "У меня возникла проблема!",
          message_date: "6:38",
          message_meridiem: "PM",
        },

        {
          id: 2,
          userName: "Andrey Sokolov",
          message: "Мне срочно нужна помощь!",
          message_date: "2:38",
          message_meridiem: "AM",
        },

        {
          id: 3,
          userName: "Vika Vishnya",
          message: "ААААААА",
          message_date: "2:38",
          message_meridiem: "AM",
        },
      ],

      messageGeneral: [
        {
          id: 1,
          userName: "Jeshua Stout",
          message: "Сообщения для воторого канала!",
          message_date: "6:38",
          message_meridiem: "PM",
        },

        {
          id: 2,
          userName: "Andrey Sokolov",
          message: "И это сообщение для воторого канала",
          message_date: "2:38",
          message_meridiem: "AM",
        },
      ],

      newMesText: "it-kamasutra.com",
    },

    friendsPage: {
      FriendsData: [
        { name: "Orlando Diggs", id: 1 },
        { name: "Carmen Velasco", id: 2 },
        { name: "Marie Jensen", id: 3 },
        { name: "Alex Lee", id: 4 },
        { name: "Leo Gill", id: 5 },
        { name: "Britney Coopers", id: 6 },
      ],

      messageFriends: [
        {
          id: 1,
          userName: "Jeshua Stout",
          message: "Сообщения для друга!",
          message_date: "6:38",
          message_meridiem: "PM",
        },
      ],

      newMessageBody: "",
    },
  },
  _callSubscriber() {
    console.log("State changed");
  },

  // Вызываем анонимный метод state
  getState() {
    return this._state;
  },
  subscribe(observer) {
    this._callSubscriber = observer;
  },

  dispatch(action) {
    this._state.messagesPage = messageReducer(this._state.messagesPage, action); //state - обновился
    this._state.friendsPage = friendsReducer(this._state.friendsPage, action); //state - обновился
    this._callSubscriber(this._state); //уведомили подписчиков
  },
};

window.store = store;

export default store;
